import prologue
import strutils
import lmdb
import base64

type
  Account = object
    username: string
    password: string

const dbPath = "./db"

proc authenticate(req: Request, env: LMDBEnv): bool =
  if req.hasHeader("Authorization"):  
    let authHeader = req.getHeader("Authorization")
  
    let credentials = authHeader[0].split(" ")
    if credentials.len == 2 and credentials[0] == "Basic":
      let decoded = decode credentials[1]
      let creds = decoded.split(":")
    
      let txn = env.newTxn()
      let dbi = txn.dbiOpen("db", cuint 0)
      var key = username.toCString()
      var value: cstring
      let result = db.get(txn, key, value)
      txn.abort()
    
      if result == LMDB_SUCCESS and password == value:
        return true
    else:
      return false
  

proc setupDatabase(): LMDBEnv =
  let env = initLMDBEnv(dbPath, flags = [lmdb.NoSync, lmdb.NoSubDir])
  return env

proc insertAccount(env: LMDBEnv, username, password: string) =
  let txn = env.beginTxn()
  let db = txn.openDB()
  db.put(txn, username.toCString(), password.toCString())
  txn.commit()

proc secure(ctx: Context) {.async.} =
  if authenticate(req, env):
    let username = req.headers.get("Authorization").get.split(" ")[1].fromBase64String().split(":")[0]
    res.send("Logged in as: " & username)
  else:
    res.status = HttpUnauthorized
    res.headers.add("WWW-Authenticate", "Basic realm=\"Secure Area\"")
    res.send("Authentication required.")

proc main() =
  let app = newApp()
  let env = setupDatabase()

  app.get("/secure", secure)

  app.run()

when isMainModule:
  let env = setupDatabase()
  insertAccount(env, "user", "password")
  main()