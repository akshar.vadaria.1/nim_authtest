# Package

version       = "0.1.0"
author        = "Akshar Vadaria"
description   = "A new awesome nimble package"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["nim_authtest"]


# Dependencies

requires "nim >= 2.0.0", "prologue", "lmdb"
